import { DATA_UPDATE } from "./constants";

export const filterParagraphs = payload => ({
  type: DATA_UPDATE,
  payload
});