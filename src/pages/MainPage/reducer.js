import {
  DATA_REQUEST,
  DATA_SUCCESS,
  DATA_ERROR,
  MOCKED_DATA
} from "./constants";

export const initialState = {
  data: MOCKED_DATA,
  error: "",
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case DATA_REQUEST:
      return {
        ...state,
        loading: true
      };

    case DATA_SUCCESS:
      return {
        ...state,
        data: [...state.data, action.payload],
        loading: false
      };

    case DATA_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };

    default:
      return state;
  }
};