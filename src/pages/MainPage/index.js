import React, {useState} from 'react'
import { shallowEqual, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

export const MainPage = () => {
  const storedData = useSelector(state => state.main.data, shallowEqual)
  
  const [data, setData] = useState(storedData)
  const handleChange = event => {
    const filterData = storedData.filter(item => item.title.includes(event.target.value) || item.body.includes(event.target.value))
    setData(filterData)
  }

  return (
    <>
      <Container>
        <FormControl>
          <InputLabel htmlFor="search-paragraph">Find the paragraph</InputLabel>
          <Input id="search-paragraph" onChange={handleChange} />
        </FormControl>
        {data.map((item, index) => (
          <div key={index} style={{marginBottom: '20px'}}>
            <Typography variant="h4" component="h4">{item.title}</Typography>
            <Typography variant="body1" component="p">{item.body}</Typography>
            <Link to={`/detailed/${item.id}`}>See more</Link>
          </div>
        ))}  
      </Container>
    </>
  )
}