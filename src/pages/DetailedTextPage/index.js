import React from 'react'
import { shallowEqual, useSelector } from 'react-redux'
import { Link, useParams } from 'react-router-dom'
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

export const DetailedTextPage = () => {
  const {id} = useParams()
  const data = useSelector(state => state.main.data.find(item => item.id === Number(id)), shallowEqual)

  return (
    <Container>
      <div style={{marginBottom: '20px'}}>
        <Typography variant="h4" component="h4">{data.title}</Typography>
        <Typography variant="body1" component="p">{data.body}</Typography>
        <Link to='/'>Go back</Link>
      </div>
    </Container>
  )
}