import React from 'react'
import {Switch, Route} from 'react-router-dom'
import { MainPage } from './pages/MainPage'
import { DetailedTextPage } from './pages/DetailedTextPage'

export const Routes = () => (
  <Switch>
    <Route exact path="/" component={MainPage} />
    <Route path="/detailed/:id" component={DetailedTextPage} />
  </Switch>
)