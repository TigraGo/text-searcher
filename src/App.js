import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom'
import { Routes } from './Routes';
import { Provider } from 'react-redux';
import { configureStore } from './redux/store';
import { CssBaseline } from '@material-ui/core';
import './App.css';

const store = configureStore()
const App = () => (
  <div className="wrapper">
    <Provider store={store}>
        <CssBaseline />
        <Router>
          <Routes />
        </Router>
    </Provider>
  </div>
)

export default App;
