import { combineReducers } from "redux";
import main from '../pages/MainPage/reducer'

const appReducer = combineReducers({
  main
});

export default (state, action = {}) =>
  appReducer(state, action);